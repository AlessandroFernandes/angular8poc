import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PhotoListComponent } from './photos/photo-list/photo-list.component';
import { NotfoundComponent } from './error/notfound/notfound.component';
import { PhotoResolve } from './photos/photo.resolve';
import { PhotoFormComponent } from './photos/photo-form/photo-form.component';
import { CurrentAuth } from './core/auth/current.auth';
import { PhotoDetailsComponent } from './photos/photo-details/photo-details.component';
import { ErroPageComponent } from './error/error-page/error-page.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule'
  },
  {
    path: 'user/:userName',
    component: PhotoListComponent,
    resolve: { photos : PhotoResolve },
    data: {
      title: 'Photo list'
    }
  },
  {
    path: 'p/add',
    component: PhotoFormComponent,
    canActivate: [ CurrentAuth ],
    data: {
      title: 'Photo form'
    }
  },
  {
    path: 'p/detail/:idPhoto',
    component: PhotoDetailsComponent,
    data: {
      title: 'Photo detail'
    }
  },
  {
    path: 'not-found',
    component: NotfoundComponent,
    data: {
      title: 'Not found'
    }
  },
  {
    path: 'error',
    component: ErroPageComponent,
    data: {
      title: 'Error'
    }
  },
  {
    path: '**',
    redirectTo: 'not-found'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
