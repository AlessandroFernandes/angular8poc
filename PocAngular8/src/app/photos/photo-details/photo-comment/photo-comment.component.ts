import { Component, Input, OnInit } from '@angular/core';
import { PhotoService } from '../../photos.service';
import { PhotoComment } from './photo-comment';
import { Observable, pipe } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-photoComment',
  templateUrl: './photo-comment.component.html',
  styleUrls: ['./photo-comment.component.css']
})
export class PhotoCommentComponent implements OnInit {

  photoComment$: Observable<PhotoComment>;
  @Input() PhotoId: number;
  formComment: FormGroup;

  constructor(private photoService: PhotoService,
              private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.photoComment$ = this.photoService.findCommentsId(this.PhotoId);
    this.formComment = this.formBuilder.group({
      comment: ['', Validators.maxLength(150)]
    });
  }

  saveComment() {
     this.photoComment$ = this.photoService
                              .addComment(this.PhotoId, this.formComment.get('comment').value as string)
                              .pipe(switchMap(
                                  () => this.photoService.findCommentsId(this.PhotoId)
                              ))
                              .pipe(tap(
                                  () => this.formComment.reset()
                              ));
}

}
