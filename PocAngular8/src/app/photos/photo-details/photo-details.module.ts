import { NgModule } from '@angular/core';
import { PhotoDetailsComponent } from './photo-details.component';
import { CommonModule } from '@angular/common';
import { PhotoModule } from '../photo/photo.module';
import { PhotoCommentComponent } from './photo-comment/photo-comment.component';
import { RouterModule } from '@angular/router';
import { VMessageModule } from 'src/app/shared/component/v-message/v-message.module';
import { ReactiveFormsModule } from '@angular/forms';
import { OwnerOnlyPhotoDirective } from './owner-only-photo/owner-only-photo.directive';
import { AlertModule } from 'src/app/shared/component/alert/alert.module';
import { ShowIfLoggedModule } from 'src/app/shared/directives/show-if-logged/show-if-logged.module';
import { ProgressModule } from 'src/app/shared/component/progress/progress.module';

@NgModule({
  declarations: [
    PhotoDetailsComponent,
    PhotoCommentComponent,
    OwnerOnlyPhotoDirective
  ],
  imports: [
    CommonModule,
    PhotoModule,
    RouterModule,
    VMessageModule,
    ReactiveFormsModule,
    AlertModule,
    ShowIfLoggedModule,
    ProgressModule
  ]
})
export class PhotoDetailsModule {}
