import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PhotoService } from '../photos.service';
import { Observable, of, throwError } from 'rxjs';
import { Photo } from '../photo';
import { UserService } from 'src/app/core/user/user.service';
import { AlertService } from 'src/app/shared/component/alert/alert.service';

@Component({
  templateUrl: './photo-details.component.html'
})
export class PhotoDetailsComponent implements OnInit {

  photo$: Observable<Photo>;
  PhotoId: number;

  constructor(private route: ActivatedRoute,
              private photoService: PhotoService,
              private router: Router,
              private userService: UserService,
              private alertService: AlertService) {}

  ngOnInit(): void {
    this.PhotoId = this.route.snapshot.params.idPhoto;
    this.photo$ = this.photoService.findPhotoById(this.PhotoId);
    this.photo$.subscribe(() => {}, err => this.router.navigate(['not-found']));
  }

  remove(): void {
    this.photoService.removePhoto(this.PhotoId)
                     .subscribe(
                       () => { this.alertService.success('Photo successfully deleted', true);
                               this.router.navigate(['/user', this.userService.getUserName()], { replaceUrl: true});
                              },
                       err => { console.log(err);
                                this.alertService.danger('Could not delete photo', true);
                              }
                     );
  }

  like(photo: Photo) {
    return this.photoService.like(photo.id)
               .subscribe(
                 () => this.photo$ = this.photoService.findPhotoById(photo.id)
               );
  }

}
