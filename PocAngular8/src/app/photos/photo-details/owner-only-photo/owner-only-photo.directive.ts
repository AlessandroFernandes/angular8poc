import { Directive, OnInit, ElementRef, Renderer2, Input } from '@angular/core';
import { UserService } from 'src/app/core/user/user.service';
import { Photo } from '../../photo';

@Directive({
  selector: '[ownerOnlyPhoto]'
})
export class OwnerOnlyPhotoDirective implements OnInit {

  @Input() ownedOnly: Photo;

  constructor(private el: ElementRef,
              private renderer: Renderer2,
              private userService: UserService) {}

  ngOnInit(): void {
    this.userService.getUser()
                    .subscribe(user => {
                      if (user.id !== this.ownedOnly.userId) {
                        this.renderer.setStyle(this.el.nativeElement, 'display', 'none');
                      }
                    });
  }

}
