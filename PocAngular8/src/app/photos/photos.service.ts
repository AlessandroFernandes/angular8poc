import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Photo } from './photo';
import { PhotoComment } from './photo-details/photo-comment/photo-comment';
import { map, catchError } from 'rxjs/operators';
import { of, throwError } from 'rxjs';
import { environment } from '../../environments/environment';

const API = environment.api;

@Injectable({ providedIn: 'root'})
export class PhotoService {

  constructor(private http: HttpClient) {}

  public PhotosUser(user: string) {
    return this.http.get<Photo[]>(`${ API }/${ user }/photos`);
  }

  public PhotoUserPaginated(user: string, page: number) {
    const params = new HttpParams().append('page', page.toString());
    return this.http.get<Photo[]>(`${ API }/${ user }/photos`, { params });
  }

  public upload(description: string, allowComments: boolean, imageFile: File) {
    const formData = new FormData();
    formData.append('description', description);
    formData.append('allowComments', allowComments ? 'true' : 'false');
    formData.append('imageFile', imageFile);
    return this.http.post(`${ API }/photos/upload`, formData, { observe: 'events', reportProgress: true });
  }

  public findPhotoById(photoId: number) {
    return this.http.get<Photo>(`${ API }/photos/${ photoId }`);
  }

  public findCommentsId(photoId: number) {
    return this.http.get<PhotoComment>(`${ API }/photos/${ photoId }/comments`);
  }

  public addComment(photoId: number, commentText: string) {
    return this.http.post(`${ API }/photos/${ photoId }/comments`, { commentText });
  }

  public removePhoto(photoId: number) {
    return this.http.delete(`${ API }/photos/${ photoId }`);
  }

  public like(photoId: number) {
    return this.http.post(`${ API }/photos/${ photoId }/like`, {}, { observe: 'response'})
               .pipe(map(like => true))
               .pipe(catchError(err => err.status === 304 ? of(false) : throwError(err)));
  }
}
