import { Resolve, ActivatedRouteSnapshot, RouterState, ActivatedRoute } from '@angular/router';
import { PhotoService } from './photos.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Photo } from './photo';

@Injectable({ providedIn: 'root'})
export class PhotoResolve implements Resolve <Observable<Photo[]>> {

  constructor(private photoService: PhotoService) {}


  // tslint:disable-next-line:max-line-length
  resolve(route: import('@angular/router').ActivatedRouteSnapshot, state: import('@angular/router').RouterStateSnapshot): Observable<Photo[]> | Observable<Observable<Photo[]>> | Promise<Observable<Photo[]>> {

    const user = route.params.userName;
    const page = 1;
    return this.photoService.PhotoUserPaginated(user, page);
  }
}
