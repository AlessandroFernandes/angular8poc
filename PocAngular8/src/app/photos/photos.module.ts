import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { PhotoListModule } from './photo-list/photo-list.module';
import { PhotoFormModule } from './photo-form/photo-form.module';
import { PhotoDetailsModule } from './photo-details/photo-details.module';

@NgModule({
  imports: [
    HttpClientModule,
    PhotoListModule,
    PhotoFormModule,
    PhotoDetailsModule
  ],
})
export class PhotosModule {}
