import { Pipe, PipeTransform } from '@angular/core';
import { Photo } from '../photo';

@Pipe ({ name: 'photoByDescription' })
export class PhotoByDescription implements PipeTransform  {
    transform(photos: Photo[], photoDescription: string) {

      photoDescription = photoDescription.toLowerCase().trim();

      if (photoDescription) {
        return photos.filter(photo => photo.description.toLowerCase().includes(photoDescription));
      } else {
        return photos;
      }
    }
}
