import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Photo } from '../photo';
import { PhotoService } from '../photos.service';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.css']
})
export class PhotoListComponent implements OnInit {

  photos: Photo[] = [];
  filter: string = '';
  load: boolean = true;
  user: string = '';
  currentPage: number = 1;

  constructor(private activatedRoute: ActivatedRoute,
              private photoService: PhotoService) { }

  ngOnInit(): void {
    //this.user = this.activatedRoute.snapshot.params.userName;
    this.activatedRoute.params.subscribe(params => {
            this.user = params.userName as string;
            this.photos = this.activatedRoute.snapshot.data.photos;
        });
  }

  loadMore(): void {
    this.photoService
        .PhotoUserPaginated(this.user, ++this.currentPage)
        .subscribe(photo => {
          this.photos = this.photos.concat(photo);

          if (!photo.length) {
            this.load = false;
          }
          this.filter = '';
        });
  }

}
