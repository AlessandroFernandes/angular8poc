import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { ButtonComponent } from './button/button.component';
import { PhotosComponent } from './photos/photos.component';
import { PhotoByDescription } from './photo-by-description.pipe';
import { PhotoListComponent } from './photo-list.component';
import { PhotoModule } from '../photo/photo.module';
import { CardModule } from 'src/app/shared/component/card/card.module';
import { SearchComponent } from './search/search.component';
import { RouterModule } from '@angular/router';
import { ProgressModule } from 'src/app/shared/component/progress/progress.module';
@NgModule({
  declarations: [
    ButtonComponent,
    PhotosComponent,
    PhotoByDescription,
    PhotoListComponent,
    SearchComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    PhotoModule,
    CardModule,
    RouterModule,
    ProgressModule
  ]
})
export class PhotoListModule {}
