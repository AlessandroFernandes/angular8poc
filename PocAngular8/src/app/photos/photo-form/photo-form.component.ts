import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PhotoService } from '../photos.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/user/user.service';
import { AlertService } from 'src/app/shared/component/alert/alert.service';
import { HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-photo-form',
  templateUrl: './photo-form.component.html',
  styleUrls: ['./photo-form.component.css']
})
export class PhotoFormComponent implements OnInit {

  photoForm: FormGroup;
  preview: string;
  file: File;
  progressUpload = 0;

  constructor(private formBuilder: FormBuilder,
              private photoService: PhotoService,
              private router: Router,
              private userService: UserService,
              private alertService: AlertService) { }

  ngOnInit() {
    this.photoForm = this.formBuilder.group({
      file: ['', Validators.required],
      description: ['', Validators.maxLength(150)],
      allowComments: [true],
    });
  }

  upload() {
    const description = this.photoForm.get('description').value;
    const allowComments = this.photoForm.get('allowComments').value;

    this.photoService.upload(description, allowComments, this.file)
                     .pipe(finalize(() => this.router.navigate(['/user', this.userService.getUserName()])))
                     .subscribe(
                       (event: HttpEvent<any>) => {
                          if (event.type === HttpEventType.UploadProgress) {
                            this.progressUpload = Math.round(100 * event.loaded / event.total);
                          } else if (event instanceof HttpResponse) {
                            this.alertService.success('Successfully added  the photo', true);
                          }
                       },
                       err => { this.alertService.danger('Could not add photo', true);
                                console.log(err);
                       });
  }

  previewImage(file: File) {
    this.file = file;
    const reader = new FileReader();
    reader.onload = (event: any) => this.preview = event.target.result;
    reader.readAsDataURL(file);
  }

}
