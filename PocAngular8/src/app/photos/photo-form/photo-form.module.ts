import { NgModule } from '@angular/core';
import { PhotoFormComponent } from './photo-form.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { VMessageModule } from 'src/app/shared/component/v-message/v-message.module';
import { PhotoModule } from '../photo/photo.module';
import { InstanteClickDirectiveModule } from 'src/app/shared/directives/instant-click/instant-click.module';
import { AlertModule } from 'src/app/shared/component/alert/alert.module';

@NgModule({
  declarations: [ PhotoFormComponent ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    VMessageModule,
    PhotoModule,
    InstanteClickDirectiveModule,
    AlertModule
  ]
})
export class PhotoFormModule {}
