import { TokenService } from "./token.service";

describe('Token service', () => {

  let tokenService;
  let token;

  beforeEach(() => {
    tokenService = new TokenService();
    token = 'token_teste';
    tokenService.setToken(token);
  });

  it('Is this service open', () => {
    expect( tokenService ).toBeTruthy();
  });

  it('Is it set a token', () => {
    expect( tokenService.hasToken() ).toBeTruthy();
  });

  it('Confirmed token value', () => {
    expect( tokenService.getToken()).toBe('token_teste');
  });

  it('Deleted token value', () => {
    expect( tokenService.deleteToken('token_teste') ).toBeFalsy();
  });

  afterEach(() => localStorage.clear() );

});
