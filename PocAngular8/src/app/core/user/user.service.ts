import { Injectable } from '@angular/core';
import { TokenService } from '../token/token.service';
import { BehaviorSubject } from 'rxjs';
import { User } from './user';
import * as token_decode from 'jwt-decode';

@Injectable({ providedIn: 'root' })
export class UserService {

  userDecode = new BehaviorSubject<User>(null);
  user: string;

  constructor(private tokenService: TokenService) {
    this.tokenService.hasToken() &&
      this.decoder();
  }

  setToken(token) {
    this.tokenService.setToken(token);
    this.decoder();
  }

  getUser() {
    return this.userDecode.asObservable();
  }

  quitUser() {
    this.tokenService.deleteToken();
    this.userDecode.next(null);
  }

  private decoder() {
    const token = this.tokenService.getToken();
    const userToken = token_decode(token) as User;
    this.user = userToken.name;
    this.userDecode.next(userToken);
  }

  hasUser() {
    return this.tokenService.hasToken();
  }

  getUserName() {
    return this.user;
  }
}
