import { UserService } from "./user.service";
import { TestBed } from '@angular/core/testing';
import { User } from './user';

describe('User service', () => {

  let serviceUser: UserService;
  // token valid - user test
  let tokenValido = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6ImZsYXZpbyIsImVtYWlsIjoiZmxhdmlvQGFsdXJhcGljLmNvbS5iciIsImlhdCI6MTU4ODU1NzUyMSwiZXhwIjoxNTg4NjQzOTIxfQ.wCcrh1jHj4TP_5OEfWnSJ9fUumn2r0FxFmLutrj911I';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ UserService ]
    });

    serviceUser = TestBed.get(UserService);
    serviceUser.setToken(tokenValido);
  });

  it('Is this service open', () => {
    expect( serviceUser ).toBeTruthy();
  });

  it('Is it set a token', () => {
    expect( serviceUser.hasUser() ).toBeTruthy();
  });

  it('The name token is true', () => {
    expect( serviceUser.getUserName() ).toBe('flavio');
  });

  it('Be returning user', () => {
    expect( serviceUser.getUser().subscribe( (user: User) => user ) ).toBeTruthy();
  });

  afterEach(() => localStorage.clear() );

});
