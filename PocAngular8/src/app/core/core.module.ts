import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { authInterceptorService } from './auth/auth.interceptor';
import { FooterComponent } from './footer/footer.component';
import { AlertModule } from '../shared/component/alert/alert.module';
import { ProgressModule } from '../shared/component/progress/progress.module';
import { MenuModule } from '../shared/component/menu/menu.module';
import { ShowIfLoggedModule } from '../shared/directives/show-if-logged/show-if-logged.module';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    AlertModule,
    ProgressModule,
    MenuModule,
    ShowIfLoggedModule
   ],
  exports: [
    HeaderComponent,
    FooterComponent
   ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: authInterceptorService,
      multi: true
    }
  ]
})
export class CoreModule {}
