import { TestBed, fakeAsync, tick } from "@angular/core/testing";
import { AuthService } from './auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UserService } from '../user/user.service';

describe('Auth service', () => {

  let authService;
  let userService;
  let serviceMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ AuthService ],
      imports: [ HttpClientTestingModule ]
    });

    authService = TestBed.get( AuthService );
    userService = TestBed.get( UserService );
    serviceMock = TestBed.get( HttpTestingController );
  });

  it('It this service open', () => {
    expect( authService ).toBeTruthy();
  });

  it('Login validator', fakeAsync(() => {
    const fakeUserMock = {
      id: 1,
      name: 'Alessandro',
      email: 'san-linux@hotmail.com'
    };

    const spy = spyOn(userService, 'setToken').and.returnValue(null);

    authService.Login('alessandro', '123456').subscribe(req => {
      expect( req.body ).toEqual( fakeUserMock );
      expect( spy ).toHaveBeenCalledWith('Test_Token');
    });

    const request = serviceMock.expectOne(req => {
      return req.method === 'POST';
    });

    request.flush(fakeUserMock, {
      headers: { 'x-access-token': 'Test_Token'}
    });

    tick(3000);
  }));

});
