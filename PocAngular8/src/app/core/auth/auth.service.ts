import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { UserService } from '../user/user.service';
import { environment } from '../../../environments/environment';

const URI = environment.api;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private httpClient: HttpClient,
               private userService: UserService) { }

  Login(userName: string, password: string) {
    return this.httpClient.post(`${ URI }/user/login`, { userName, password}, { observe: 'response'} )
               .pipe(tap(
                  resp => {
                    const token = resp.headers.get('x-access-token');
                    this.userService.setToken(token);
                  }
               ));
  }


}
