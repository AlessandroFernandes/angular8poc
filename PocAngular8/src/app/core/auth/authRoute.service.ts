import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from '../user/user.service';

@Injectable({ providedIn: 'root'})
export class AuthRoute implements CanActivate {

  constructor(private userService: UserService,
              private route: Router) {}

  canActivate(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
    if (this.userService.hasUser()) {
      this.route.navigate(['user', this.userService.getUserName()]);
      return false;
    }
    return true;
  }

}
