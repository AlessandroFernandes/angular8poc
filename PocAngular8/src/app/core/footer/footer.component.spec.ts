import { TestBed, async } from "@angular/core/testing";
import { FooterComponent } from './footer.component';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../user/user.service';
import { of } from 'rxjs';

describe('Footer Component', () => {

  let component: FooterComponent;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterComponent ],
      imports: [ RouterTestingModule ],
      providers: [ UserService ]
    }).compileComponents();
  })
  );

  beforeEach(() => {
    userService = TestBed.get( UserService );
    spyOn( userService, 'getUser').and.returnValue(
      of({
        email: 'san-linux@hotmail.com',
        name: 'Alessandro Fernandes',
        id: 1
      })
    );

    const featureFooter = TestBed.createComponent( FooterComponent );
    component = featureFooter.componentInstance;
    featureFooter.detectChanges();
  });

  it('Component started', () => {
    expect( component ).toBeTruthy();
  });

});
