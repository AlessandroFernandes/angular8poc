import { async, TestBed } from "@angular/core/testing";
import { HeaderComponent } from './header.component';
import { UserService } from '../user/user.service';
import { MenuModule } from 'src/app/shared/component/menu/menu.module';
import { AlertModule } from 'src/app/shared/component/alert/alert.module';
import { ProgressModule } from 'src/app/shared/component/progress/progress.module';
import { ProgressService } from 'src/app/shared/component/progress/progress.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { of } from 'rxjs';

describe('Header Component', () => {

  let component;
  let userService: UserService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeaderComponent
       ],
      imports: [
        RouterTestingModule.withRoutes([]),
        MenuModule,
        AlertModule,
        ProgressModule
       ],
      providers: [
        UserService,
        ProgressService
       ]
    }).compileComponents();
  })
  );

  beforeEach(() => {
    userService = TestBed.get( UserService );
    router = TestBed.get( Router );
    const featureHeader = TestBed.createComponent( HeaderComponent );
    component = featureHeader.componentInstance;
    featureHeader.detectChanges();

    spyOn( userService, 'getUser' ).and.returnValue(
      of({
        email: 'san-linux@hotmail.com',
        name: 'Alessandro Fernandes',
        id: 1
      })
    );
  });

  it('Component started', () => {
    expect( component ).toBeTruthy();
  });

  it('Logout is ok', () => {
    const spyUser = spyOn( userService, 'quitUser').and.returnValue(null);
    const spyRouter = spyOn( router, 'navigate');
    component.logout();
    expect( spyUser ).toHaveBeenCalled();
    expect( spyRouter ).toHaveBeenCalledWith(['']);
  });

});
