import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../user/user';
import { UserService } from '../user/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  user$ = new Observable<User>();

  constructor(private userService: UserService,
              private router: Router) {}

  ngOnInit(): void {
    this.user$ = this.userService.getUser();
  }

  logout(): void {
    this.userService.quitUser();
    this.router.navigate(['']);
  }

}
