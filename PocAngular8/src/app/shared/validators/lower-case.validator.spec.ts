import { LowerCaseValidator } from "./lower-case.validator";
import { FormControl } from '@angular/forms';

const user = new FormControl('alessandro2020');
const user2 = new FormControl('2020alessandro');
const user3 = new FormControl('-alessandro2020');
const user4 = new FormControl('Alessandro2020');

describe('Lower case Validator', () => {

  it('Has the lower case beginning', () => {
    expect(LowerCaseValidator(user)).toBeNull();
  });

  it('Has the number beginning and lower case', () => {
    expect(LowerCaseValidator(user2)).toBeNull();
  });

  it('Has the hyphen beginning and lower case', () => {
    expect(LowerCaseValidator(user3)).toBeNull();
  });

  it('Character error', () => {
    expect(LowerCaseValidator(user4)).toBeTruthy();
  });

});
