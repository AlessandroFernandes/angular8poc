import { NgModule } from '@angular/core';
import { CardComponent } from './card.component';
import { CommonModule } from '@angular/common';
import { HoverBrightnessDirectiveModule } from '../../directives/holverBrightness/hover-brightness.module';

@NgModule({
  declarations: [ CardComponent ],
  exports: [ CardComponent ],
  imports: [
    CommonModule,
    HoverBrightnessDirectiveModule
  ]
})
export class CardModule {}
