import { NgModule } from '@angular/core';
import { ProgressComponent } from './progress.component';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProgressInterceptor } from './progress.interceptor';

@NgModule({
  declarations: [
    ProgressComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ProgressComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ProgressInterceptor,
      multi: true
    }
]
})
export class ProgressModule {}
