import { Component, OnInit } from '@angular/core';
import { ProgressService } from './progress.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {

  progress$ = new Observable<string>();

  constructor(private progressService: ProgressService) {}

  ngOnInit(): void {
    this.progress$ = this.progressService.getProgress()
                        .pipe(map(type => type.valueOf()));
  }

}
