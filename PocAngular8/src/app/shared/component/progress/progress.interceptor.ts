import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpResponse } from '@angular/common/http';
import { ProgressService } from './progress.service';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root'})
export class ProgressInterceptor implements HttpInterceptor {

  constructor(private progressService: ProgressService) {}
  intercept(req: import("@angular/common/http").HttpRequest<any>,
            next: import("@angular/common/http").HttpHandler): import("rxjs").Observable<import("@angular/common/http").HttpEvent<any>> {

    return next.handle(req)
               .pipe(tap(event => {
                 if (event instanceof HttpResponse) {
                  this.progressService.done();
                 } else {
                   this.progressService.load();
                 }
               }));
  }
}
