export enum ProgressType {
  LOADING = 'loading',
  STOPPED = 'stopped'
}
