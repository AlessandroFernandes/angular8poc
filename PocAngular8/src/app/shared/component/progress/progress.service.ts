import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ProgressType } from './progress-type';
import { startWith } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ProgressService {

  progress = new Subject<ProgressType>();

  getProgress() {
    return this.progress.asObservable()
                        .pipe(startWith(ProgressType.STOPPED));
  }

  load() {
    return this.progress.next(ProgressType.LOADING);
  }

  done() {
    return this.progress.next(ProgressType.STOPPED);
  }
}
