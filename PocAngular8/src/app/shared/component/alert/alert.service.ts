import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Alert, TypeAlert } from './alert';
import { Router, NavigationStart } from '@angular/router';

@Injectable({ providedIn: 'root'})
export class AlertService {

  alertSubject: Subject<Alert> = new Subject<Alert>();
  keepAfertRouterChange = false;

  constructor(router: Router) {
    router.events.subscribe(event => {
        if (event instanceof NavigationStart) {
            if (this.keepAfertRouterChange) {
              this.keepAfertRouterChange = false;
            } else {
              this.clearAlert();
            }
        }
      }
    );
  }

  private setAlert(alertType: TypeAlert, message: string, showAfter: boolean) {
    this.keepAfertRouterChange = showAfter;
    return this.alertSubject.next(new Alert(alertType, message));
  }

  success(message: string, showAfter: boolean = false) {
    return this.setAlert(TypeAlert.SUCCESS, message, showAfter);
  }

  danger(message: string, showAfter: boolean = false) {
    return this.setAlert(TypeAlert.DANGER, message, showAfter);
  }

  info(message: string, showAfter: boolean = false) {
    return this.setAlert(TypeAlert.INFO, message, showAfter);
  }

  warning(message: string, showAfter: boolean = false) {
    return this.setAlert(TypeAlert.WARNING, message, showAfter);
  }

  getAlert() {
    return this.alertSubject.asObservable();
  }

  clearAlert(): void {
    this.alertSubject.next(null);
  }
}
