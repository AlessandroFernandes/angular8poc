import { Component, Input } from '@angular/core';
import { AlertService } from './alert.service';
import { Alert, TypeAlert } from './alert';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html'
})
export class AlertComponent {

  @Input() timeOut = 3000;
  alerts: Alert[] = [];

  constructor(private alertService: AlertService) {

    this.alertService.getAlert()
                     .subscribe(alert => {
                        if (!alert) {
                            this.alerts = [];
                            return;
                        }
                        this.alerts.push(alert);
                        setTimeout(() => this.removeAlert(alert), this.timeOut);
                     });
  }

  removeAlert(alertList: Alert) {
    this.alerts = this.alerts.filter(alert => alertList !== alert);
  }

  getAlertClass(alert: Alert) {
    if (!alert) {
      return '';
    }
    switch (alert.typeAlert) {
      case TypeAlert.SUCCESS:
        return 'alert alert-success';
      case TypeAlert.DANGER:
        return 'alert alert-danger';
      case TypeAlert.INFO:
        return 'alert alert-info';
      case TypeAlert.WARNING:
        return 'alert alert-warning';
      }
  }

}
