import { NgModule } from '@angular/core';
import { InstantClick } from './instant-click.directive';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    InstantClick
  ],
  imports: [
    CommonModule
  ],
  exports: [
    InstantClick
  ]
})
export class InstanteClickDirectiveModule {}
