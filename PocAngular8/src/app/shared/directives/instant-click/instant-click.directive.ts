import { Directive, ElementRef, Renderer2, OnInit } from '@angular/core';
import { PlatformDetectorService } from 'src/app/core/browserDetector/platform-detector.service';

@Directive({
  selector: '[instantClick]'
})
export class InstantClick implements OnInit {

  constructor(private el: ElementRef,
              private render: Renderer2,
              private platformDetector: PlatformDetectorService) {}

  ngOnInit(): void {
    this.platformDetector.isPlatform() && this.immediateClick();
  }

  immediateClick() {
    const event: any = new MouseEvent('click', this.el.nativeElement.click());
    event.stopPropagation();
    this.render.listen(this.el.nativeElement, 'dispatchEvent', event);
  }
}
