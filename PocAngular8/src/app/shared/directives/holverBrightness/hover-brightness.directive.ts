import { Directive, ElementRef, Input, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[hoverBrigtness]'
})
export class HoverBrightnessDirective {

  constructor(
    private el: ElementRef,
    private render: Renderer2
  ) {}

  @Input() levelBright: string = '70%';

  @HostListener('mouseover')
  brightOn(): void {
    this.render.setStyle(this.el.nativeElement, 'filter', 'brightness(70%)');
  }

  @HostListener('mouseleave')
  brightOff(): void {
    this.render.setStyle(this.el.nativeElement, 'filter', 'brightness(100%)');
  }
}
