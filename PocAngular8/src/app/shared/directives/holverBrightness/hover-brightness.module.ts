import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HoverBrightnessDirective } from './hover-brightness.directive';

@NgModule({
  declarations: [ HoverBrightnessDirective ],
  imports: [
    CommonModule
  ],
  exports: [ HoverBrightnessDirective ]
})
export class HoverBrightnessDirectiveModule {}
