import { Directive, ElementRef, Renderer2, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/user/user.service';

@Directive({
  selector: '[showIfLogged]',
})
export class ShowIfLoggedDirective implements OnInit {

  memoryDisplay: string;

  constructor(private el: ElementRef,
              private render: Renderer2,
              private userService: UserService) {}

  ngOnInit(): void {
    // tslint:disable-next-line:no-unused-expression
    this.memoryDisplay = getComputedStyle(this.el.nativeElement).display;
    this.userService.getUser().subscribe(user => {
      if (user) {
        this.render.setStyle(this.el.nativeElement, 'display', this.memoryDisplay);
      } else {
        this.memoryDisplay = getComputedStyle(this.el.nativeElement).display;
        this.render.setStyle(this.el.nativeElement, 'display', 'none');
      }
    });
  }
}
