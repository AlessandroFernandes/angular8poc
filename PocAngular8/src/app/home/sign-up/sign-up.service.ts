import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NewUser } from './newUser';

const API = "http://localhost:3000";

@Injectable()
export class SignupService {

  constructor(private http: HttpClient) {}

  existingUser(userName) {
    return this.http.get(`${API}/user/exists/${userName}`);
  }

  addUser(newUser: NewUser) {
    return this.http.post(`${API}/user/signup`, newUser);
  }
}
