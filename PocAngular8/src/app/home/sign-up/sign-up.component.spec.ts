import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { SignUpComponent } from './sign-up.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SignupValidatorService } from './sign-up-validator.service';
import { SignupService } from './sign-up.service';
import { ReactiveFormsModule, FormControl } from '@angular/forms';
import { VMessageModule } from 'src/app/shared/component/v-message/v-message.module';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';

describe('Signup component', () => {

  let component: SignUpComponent;
  let signupService: SignupService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpComponent ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
        ReactiveFormsModule,
        VMessageModule
      ],
      providers: [
        SignupValidatorService,
        SignupService
      ]
    }).compileComponents();
  })
  );

  beforeEach(() => {
    const featureSigup = TestBed.createComponent( SignUpComponent );
    component = featureSigup.componentInstance;
    featureSigup.detectChanges();

    router = TestBed.get( Router );
    signupService = TestBed.get( SignupService );
  });

  it('Component started', () => {
    expect( component ).toBeTruthy();
  });

  it('User registration', () => {
    const spyrouter = spyOn( router, 'navigate');
    spyOn( signupService, 'addUser').and.returnValue(of(true));
    component.signUpForm.get( 'email' ).setValue('san-linux@hotmail.com');
    component.signUpForm.get( 'fullName' ).setValue('Alessandro Fernandes');
    component.signUpForm.get( 'userName' ).setValue('sandrofernandes');
    component.signUpForm.get( 'password' ).setValue('sandro123');
    const teste = component.signUp();
    expect( teste ).toBeFalsy();
    //expect( spyrouter ).toHaveBeenCalledWith(['']);
  });

  it('Erro server test', () => {
    spyOn( signupService, 'addUser').and.returnValue(throwError('Server error'));
    const log = spyOn(console, 'log');
    component.signUpForm.get( 'email' ).setValue('san-linux@hotmail.com');
    component.signUpForm.get( 'fullName' ).setValue('Alessandro Fernandes');
    component.signUpForm.get( 'userName' ).setValue('sandrofernandes');
    component.signUpForm.get( 'password' ).setValue('sandro123');
    expect( component.signUp() ).toBeFalsy();
    //expect( log ).toHaveBeenCalledWith('Server error');
  });

});
