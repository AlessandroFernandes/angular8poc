import { Injectable } from '@angular/core';
import { SignupService } from './sign-up.service';
import { AbstractControl } from '@angular/forms';
import { debounceTime, switchMap, first, map } from 'rxjs/operators';

@Injectable()
export class SignupValidatorService {

  constructor(private signupService: SignupService) {}

  validatorExistingUser() {
    return (control: AbstractControl) => {
      return control
                    .valueChanges
                    .pipe(debounceTime(300))
                    .pipe(switchMap(userName => this.signupService.existingUser(userName)))
                    .pipe(map(isUser => isUser ? isUser = { existingUser: true} : null))
                    .pipe(first());
    };
  }
}
