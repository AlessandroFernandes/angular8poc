import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LowerCaseValidator } from 'src/app/shared/validators/lower-case.validator';
import { SignupValidatorService } from './sign-up-validator.service';
import { NewUser } from './newUser';
import { SignupService } from './sign-up.service';
import { Router } from '@angular/router';
import { userNamePasswordValidator } from './userName.password.validator';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  providers: [ SignupValidatorService ]
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private signupValidatorService: SignupValidatorService,
              private signupService: SignupService,
              private router: Router) { }

  ngOnInit() {
    this.signUpForm = this.formBuilder.group({
      email: ['',
        [
          Validators.required,
          Validators.email
        ]
    ],
      fullName: ['',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(50)
        ]
    ],
      userName: ['',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(15),
          LowerCaseValidator
        ],
        this.signupValidatorService.validatorExistingUser()
    ],
      password: ['',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(10)
        ]
    ]
    },
    {
      validators: userNamePasswordValidator
    }
    );
  }

  signUp() {
    if (this.signUpForm.valid && !this.signUpForm.pending) {
      const newUser = this.signUpForm.getRawValue() as NewUser;
      this.signupService.addUser(newUser)
          .subscribe(
            () => this.router.navigate(['']),
            erro => console.log(erro)
          );
    }
  }
}
