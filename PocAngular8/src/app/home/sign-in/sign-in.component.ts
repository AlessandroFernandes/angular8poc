import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PlatformDetectorService } from 'src/app/core/browserDetector/platform-detector.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  formSigh: FormGroup;
  urlHistory: string;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private activatedRouter: ActivatedRoute,
              private renderer: Renderer2,
              private platformDectorService: PlatformDetectorService) { }

  ngOnInit() {
    this.activatedRouter.queryParams.subscribe(param => this.urlHistory = param.fromUrl);

    this.formSigh = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  Load(e: Event) {
    e.preventDefault();
    const user = this.formSigh.get('userName').value;
    const password = this.formSigh.get('password').value;

    this.authService
        .Login(user, password)
        .subscribe(
          () => {
            this.urlHistory ? this.router.navigateByUrl(this.urlHistory) : this.router.navigate(['user', user]);
          },
          err => { console.log(err),
                   this.formSigh.reset();
                   this.platformDectorService.isPlatform() &&
                    this.renderer.selectRootElement('#userNameInput').focus();
          }
        );
  }

}
