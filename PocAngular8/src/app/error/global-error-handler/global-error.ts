export interface GlobalError {
  traceError: string;
  message: string;
  user: string;
  path: string;
}
