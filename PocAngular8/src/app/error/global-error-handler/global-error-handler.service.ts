import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { GlobalError } from './global-error';

@Injectable()
export class GlobalErrorHandlerService {

  private API = environment.logger;

  constructor(private http: HttpClient) {}

  sendLogger(globalError: GlobalError) {
    return this.http.post<GlobalError>(`${ this.API }/infra/log`, globalError);
  }
}
