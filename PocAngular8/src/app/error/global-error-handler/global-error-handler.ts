import { ErrorHandler, Injectable, Injector } from '@angular/core';
import * as StackTrace from 'stacktrace-js';
import { LocationStrategy } from '@angular/common';
import { UserService } from 'src/app/core/user/user.service';
import { GlobalErrorHandlerService } from './global-error-handler.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private injector: Injector) {}
  handleError(error: any): void {

    const location = this.injector.get(LocationStrategy);

    const path = location instanceof LocationStrategy ? location.path() : '';
    const userService = this.injector.get(UserService);
    const logService = this.injector.get(GlobalErrorHandlerService);
    const router = this.injector.get(Router);
    const message = error.message ? error.message : error.toString();

    StackTrace.fromError(error)
              .then(erro => {
                const trace = erro.map(sk => sk.toString())
                                   .join('\n');

                if (environment.production) { router.navigate(['/error']); }

                logService.sendLogger({ traceError: trace, message, user: userService.getUserName(), path })
                           .subscribe(
                             () => {
                                console.log('***** Captured error handler *****');
                                console.log({ trace,
                                              message,
                                              user: userService.getUserName(),
                                              path
                                            });
                             },
                             err => console.log('Could not send logger!')
                           );
              });
  }
}
