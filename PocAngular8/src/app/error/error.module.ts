import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotfoundComponent } from './notfound/notfound.component';
import { GlobalErrorHandler } from './global-error-handler/global-error-handler';
import { GlobalErrorHandlerService } from './global-error-handler/global-error-handler.service';
import { RouterModule } from '@angular/router';
import { ErroPageComponent } from './error-page/error-page.component';

@NgModule({
  declarations: [
    NotfoundComponent,
    ErroPageComponent
  ],
  imports: [
     CommonModule,
     RouterModule
  ],
  providers: [
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    },
    GlobalErrorHandlerService
  ]
})
export class ErrorModule { }
